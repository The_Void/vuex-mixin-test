export const user = {
    namespaced: true,
    state: {
        name: '',
        email: '',
        password: ''
    },
    mutations: {
        UPDATE_NAME(state, payload) {
            state.name = payload;
        },
        UPDATE_EMAIL(state, payload) {
            state.email = payload;
        },
        UPDATE_PASSWORD(state, payload) {
            state.password = payload;
        },
    },
    actions: {

    }
}