import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  mounted() {
    this.$router.replace('/');
  },
  render: h => h(App)
}).$mount('#app')
