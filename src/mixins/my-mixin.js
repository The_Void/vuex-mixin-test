import { mapState } from 'vuex'  // Add mapMutations

export const myMixin = {
	data() {
		return {};
	},
	computed: {
		...mapState('user', [
			'name',
			'email',
			'password'
		])
	},
	methods: {
		greetings() {
			console.log(this.email);
		}
	}
};